﻿using CloudFlare.API.Data;
using CloudFlare.API.Enums;
using CloudFlare.API.Utils;
using RestSharp;

namespace CloudFlare.API.Methods
{
    /// <summary>
    /// Proxy class for DNS related requests
    /// </summary>
    public class Dns
    {
        #region " Attributes "

        /// <summary>
        /// The request
        /// </summary>
        private static readonly RestRequest request = new RestRequest(Method.POST);

        #endregion " Attributes "

        #region " Public methods "

        /// <summary>
        /// Create a DNS record for a zone.
        /// http://www.cloudflare.com/docs/client-api.html#s5.1
        /// </summary>
        /// <param name="name">Name of the DNS record.</param>
        /// <param name="content">The content of the DNS record, will depend on the the type of record being added.</param>
        /// <param name="domain">The target domain.</param>
        /// <param name="type">Type of DNS record. Values include: [A/CNAME/MX/TXT/SPF/AAAA/NS/SRV/LOC]. Available at {CloudFlare.API.Enums.DnsRecordKind}</param>
        /// <param name="ttl">TTL of record in seconds. 1 = Automatic, otherwise, value must in between 120 and 4,294,967,295 seconds.</param>
        /// <returns>A {DnsObject} with the new data created, or NULL if it was not successfull</returns>
        /// 9/5/2013 by Sergi
        public DnsObject Add(string name, string content, string domain, DnsRecordKind type, int ttl = 1)
        {
            request.AddParameter(CFParameters.Method, CFMethods.AddRecord);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.Type, type.ToString());
            request.AddParameter(CFParameters.Name, name);
            request.AddParameter(CFParameters.Content, content);
            request.AddParameter(CFParameters.TTL, ttl);

            var response = CFProxy.Execute<DnsResponse>(request);
            return response.response.rec.obj;
        }

        /// <summary>
        /// Edit a DNS record for a zone
        /// http://www.cloudflare.com/docs/client-api.html#s5.2
        /// </summary>
        /// <param name="id">The unique identifier of the record.</param>
        /// <param name="name">Name of the DNS record.</param>
        /// <param name="content">The content of the DNS record, will depend on the the type of record being added</param>
        /// <param name="domain">The target domain.</param>
        /// <param name="type">Type of DNS record. Values include: [A/CNAME/MX/TXT/SPF/AAAA/NS/SRV/LOC]. Available at {CloudFlare.API.Enums.DnsRecordKind}</param>
        /// <param name="service_mode">Status of CloudFlare Proxy, 1 = orange cloud, 0 = grey cloud.</param>
        /// <param name="ttl">TTL of record in seconds. 1 = Automatic, otherwise, value must in between 120 and 4,294,967,295 seconds.</param>
        /// <returns>A {DnsObject} with the new data created, or NULL if it was not successfull</returns>
        /// 9/5/2013 by Sergi
        public DnsObject Edit(string id, string name, string content, string domain, DnsRecordKind type, ServiceMode service_mode, int ttl = 1)
        {
            request.AddParameter(CFParameters.Method, CFMethods.EditRecord);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.Id, id);
            request.AddParameter(CFParameters.Type, type.ToString());
            request.AddParameter(CFParameters.Name, name);
            request.AddParameter(CFParameters.Content, content);
            request.AddParameter(CFParameters.TTL, ttl);
            if (type == DnsRecordKind.A || type == DnsRecordKind.AAAA || type == DnsRecordKind.CNAME)
            {
                request.AddParameter(CFParameters.ServiceMode, (int)service_mode);
            }

            var response = CFProxy.Execute<DnsResponse>(request);
            return response.response.rec.obj;
        }

        /// <summary>
        /// Deletes the DNS record.
        /// http://www.cloudflare.com/docs/client-api.html#s5.3
        /// </summary>
        /// <param name="id">The unique identifier of the record.</param>
        /// <param name="domain">The target domain.</param>
        /// <returns></returns>
        /// 9/5/2013 by Sergi
        public bool Delete(string id, string domain)
        {
            request.AddParameter(CFParameters.Method, CFMethods.DeleteRecord);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.Id, id);

            var response = CFProxy.Execute<DnsResponse>(request);
            return response.result == ResultKind.Success.GetStringValue();
        }

        #endregion " Public methods "
    }
}
