﻿using System;
using System.Globalization;
using System.Linq;
using CloudFlare.API.Data;
using CloudFlare.API.Enums;
using CloudFlare.API.Utils;
using RestSharp;

namespace CloudFlare.API.Methods
{
    /// <summary>
    /// Proxy class for modification related requests
    /// </summary>
    /// 9/5/2013 by Sergi
    public class Modify
    {
        #region " Attributes "

        /// <summary>
        /// The request
        /// </summary>
        private static readonly RestRequest request = new RestRequest(Method.POST);

        #endregion " Attributes "

        #region " Public methods "

        /// <summary>
        /// This function sets the Basic Security Level to I'M UNDER ATTACK! / HIGH / MEDIUM / LOW / ESSENTIALLY OFF.
        /// http://www.cloudflare.com/docs/client-api.html#s4.1
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="securityLevel">The security level. Available at {CloudFlare.API.Enums.SecurityLevel}</param>
        /// <returns>True if the request was successful</returns>
        /// 9/7/2013 by Sergi
        public bool SetSecurityLevel(string domain, SecurityLevel securityLevel)
        {
            return SimpleRequest(CFMethods.SecurityLevel, domain, securityLevel.GetStringValue());
        }

        /// <summary>
        /// Sets the cache level.
        /// This function sets the Caching Level to Aggressive or Basic.
        /// http://www.cloudflare.com/docs/client-api.html#s4.2
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="cacheLevel">The cache level. Available at {CloudFlare.API.Enums.CacheLevel}</param>
        /// <returns>True if the request was successful</returns>
        /// 9/7/2013 by Sergi
        public bool SetCacheLevel(string domain, CacheLevel cacheLevel)
        {
            return SimpleRequest(CFMethods.CacheLevel, domain, cacheLevel.GetStringValue());
        }

        /// <summary>
        /// Sets the developer mode.
        /// http://www.cloudflare.com/docs/client-api.html#s4.3
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="mode">The mode. Available at {CloudFlare.API.Enums.DeveloperLevel}</param>
        /// <returns>A {ZoneResult} object with data about the domain</returns>
        /// 9/7/2013 by Sergi
        public ZoneResult SetDeveloperMode(string domain, DeveloperMode mode)
        {
            request.AddParameter(CFParameters.Method, CFMethods.DevMode);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.DevMode, (int)mode);

            var response = CFProxy.Execute<ZoneResponse>(request);
            return response.result == ResultKind.Success.GetStringValue() ? response.response : null;
        }

        /// <summary>
        /// Clear CloudFlare's cache.
        /// This function will purge CloudFlare of any cached files. It may take up to 48 hours for the cache to rebuild and 
        /// optimum performance to be achieved so this function should be used sparingly.
        /// http://www.cloudflare.com/docs/client-api.html#s4.4
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <returns>A {PurgeCacheResponse} object with data about the domain</returns>
        /// 9/7/2013 by Sergi
        public PurgeCacheResponse PurgeCache(string domain)
        {
            request.AddParameter(CFParameters.Method, CFMethods.PurgeCache);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.PurgeCache, 1);

            var response = CFProxy.Execute<PurgeCacheResponse>(request);
            return response.result == ResultKind.Success.GetStringValue() ? response : null;
        }

        /// <summary>
        /// Purge a single file in CloudFlare's cache.
        /// http://www.cloudflare.com/docs/client-api.html#s4.5
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="url">
        ///     The full URL of the file that needs to be purged from Cloudflare's cache. 
        ///     Keep in mind, that if an HTTP and an HTTPS version of the file exists, then both versions 
        ///     will need to be purged independently</param>
        /// <returns>A {SingleFilePurgeCacheResult} object with data about the file purged</returns>
        /// 9/7/2013 by Sergi
        public SingleFilePurgeCacheResult PurgeFileCache(string domain, string url)
        {
            request.AddParameter(CFParameters.Method, CFMethods.PurgeFile);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.Url, url);

            var response = CFProxy.Execute<SingleFilePurgeCacheResponse>(request);
            return response.result == ResultKind.Success.GetStringValue() ? response.response : null;
        }

        /// <summary>
        /// Update the snapshot of site for CloudFlare's challenge page. Tells CloudFlare to take a new image of your site.
        /// http://www.cloudflare.com/docs/client-api.html#s4.6
        /// </summary>
        /// <param name="zone_id">ID of zone, found with CFProxy.Access.ZoneCheck()</param>
        /// <returns>True if the request was successful</returns>
        /// 9/8/2013 by Sergi
        public bool UpdateSnapshot(string zone_id)
        {
            request.AddParameter(CFParameters.Method, CFMethods.ZoneGrab);
            request.AddParameter(CFParameters.Zones, zone_id);

            var response = CFProxy.Execute<SimpleResponse>(request);
            return response.result == ResultKind.Success.GetStringValue();
        }

        /// <summary>
        /// Sets the ip restriction. Whitelist/Blacklist/Unlist IPs
        /// http://www.cloudflare.com/docs/client-api.html#s4.7
        /// </summary>
        /// <param name="ip">The IP address you want to whitelist/blacklist</param>
        /// <param name="level">The restriction level. Available at {CloudFlare.API.Enums.IPRestrictionLevel}</param>
        /// <returns></returns>
        /// 9/7/2013 by Sergi
        public IPRestriction SetIPRestriction(string ip, IPRestrictionLevel level)
        {
            request.AddParameter(CFParameters.Method, level.GetStringValue());
            request.AddParameter(CFParameters.Key, ip);

            var response = CFProxy.Execute<IPRestrictionResponse>(request);
            return response.result == ResultKind.Success.GetStringValue() ? response.response.result : null;
        }

        /// <summary>
        /// Sets the IPV6 support.
        /// http://www.cloudflare.com/docs/client-api.html#s4.8
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="mode">The mode. Available at {CloudFlare.API.Enums.IPV6Mode}</param>
        /// <returns>True if the request was successful</returns>
        /// 9/7/2013 by Sergi
        public bool SetIPV6(string domain, IPV6Mode mode)
        {
            return SimpleRequest(CFMethods.SetIPV6, domain, ((int)mode).ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Sets the rocket loader mode.
        /// http://www.cloudflare.com/docs/client-api.html#s4.9
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="mode">The mode. Available modes at {CloudFlare.API.Enums.RocketLoaderMode}</param>
        /// <returns>True if the request was successful</returns>
        /// 9/7/2013 by Sergi
        public bool SetRocketLoader(string domain, RocketLoaderMode mode)
        {
            return SimpleRequest(CFMethods.Async, domain, mode.GetStringValue());
        }

        /// <summary>
        /// Sets the minification mode
        /// http://www.cloudflare.com/docs/client-api.html#s4.10
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="mode">The mode. Available modes at {CloudFlare.API.Enums.MinifyMode}</param>
        /// <returns>True if the request was successful</returns>
        /// 9/7/2013 by Sergi
        public bool SetMinify(string domain, MinifyMode mode)
        {
            return SimpleRequest(CFMethods.Minify, domain, ((int)mode).ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Sets the Mirage2 support.
        /// http://www.cloudflare.com/docs/client-api.html#s4.11
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="mode">The mode. Available at {CloudFlare.API.Enums.MirageMode}</param>
        /// <returns>True if the request was successful</returns>
        /// 9/7/2013 by Sergi
        public bool SetMirage(string domain, MirageMode mode)
        {
            return SimpleRequest(CFMethods.Mirage2, domain, ((int)mode).ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Enables/disables the cloudflare proxy for the given dns record in domain.
        /// </summary>
        /// <param name="record">The DNS record.</param>
        /// <param name="domain">The domain.</param>
        /// <param name="mode">The mode. Available at {CloudFlare.API.Enums.ServiceMode}</param>
        /// <returns></returns>
        /// 9/9/2013 by Sergi
        public bool EnableCloudflare(string record, string domain, ServiceMode mode)
        {
            var access = new Access();
            var records = access.GetRecords(domain);
            DnsObject entry = null;
            foreach (var dnsObject in records.Where(dnsObject => dnsObject.name.Equals(record)))
            {
                entry = dnsObject;
            }

            if (entry != null)
            {
                var DNS = new Dns();
                var ttl = 1;
                Int32.TryParse(entry.ttl, out ttl);
                DNS.Edit(entry.rec_id, record, entry.content, entry.zone_name, EnumExtensions.GetValue<DnsRecordKind>(entry.type), mode, ttl);
                return true;
            }

            var proxyException = new ApplicationException(String.Format("The record \"{0}\" was not found in domain \"{1}\" zone", record, domain));
            throw proxyException;
        }

        #endregion " Public methods "

        #region " Private methods "

        /// <summary>
        /// Common request.
        /// </summary>
        /// <param name="a">The method to call.</param>
        /// <param name="domain">The domain.</param>
        /// <param name="p">Additional parameter</param>
        /// <returns></returns>
        /// 9/13/2013 by Sergi
        private bool SimpleRequest(string a, string domain, string p)
        {
            request.AddParameter(CFParameters.Method, a);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.V, p);

            var response = CFProxy.Execute<SimpleResponse>(request);
            return response.result == ResultKind.Success.GetStringValue();
        }

        #endregion " Private methods "
    }
}
