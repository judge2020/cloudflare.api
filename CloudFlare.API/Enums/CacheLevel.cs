﻿using CloudFlare.API.Utils;

namespace CloudFlare.API.Enums
{
    /// <summary>
    /// The available security levels
    /// </summary>
    /// 9/7/2013 by Sergi
    public enum CacheLevel
    {
        [StringValue("agg")]
        Aggressive,

        [StringValue("basic")]
        Basic
    }
}
