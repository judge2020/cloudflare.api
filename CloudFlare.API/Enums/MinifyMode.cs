﻿namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Rocket Loader Modes
    /// </summary>
    public enum MinifyMode
    {
        OFF = 0,
        JS = 1,
        CSS = 2,
        JS_CSS = 3,
        HTML = 4,
        JS_HTML = 5,
        CSS_HTML = 6,
        JS_CSS_HTML = 7
    }
}
