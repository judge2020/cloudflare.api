﻿namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Kind of DNS Record
    /// </summary>
    public enum DnsRecordKind
    {
        Error,
        A,
        CNAME,
        MX,
        TXT,
        SPF,
        AAAA,
        NS,
        SRV,
        LOC
    }
}
