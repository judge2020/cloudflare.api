﻿using CloudFlare.API.Utils;

namespace CloudFlare.API.Enums
{
    /// <summary>
    /// The available security levels
    /// </summary>
    /// 9/7/2013 by Sergi
    public enum SecurityLevel
    {
        [StringValue("help")]
        UnderAttack,

        [StringValue("high")]
        High,

        [StringValue("med")]
        Medium,

        [StringValue("low")]
        Low,

        [StringValue("eoff")]
        Off
    }
}
